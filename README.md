Для того чтобы подключить в composer.json проекта прописать:
```composer
"repositories": [
    {
        "type": "package",
        "package": {
            "name": "sts-calc/sts-calc",
            "version": "1.0",
            "type": "symfony-bundle",
            "source": {
                "url": "https://gitlab.com/ashur.umarov/sts.git",
                "type": "git",
                "reference": "master"
            },
            "autoload": {
                "classmap": ["/"]
            }
        }
    }
]
```
После:
```composer
composer require "sts-calc/sts-calc"
```

Добавить в `bundles.php`:
```php
STS\CalcBundle\CalcBundle::class => ['all' => true],
```

После нужно добавить файл в config/routes - sts-calc.yaml для того, чтобы заработал дефолтный контроллер:
```yaml
sts-calc:
  resource: "@CalcBundle/Controller/CalcController.php"
  type: annotation
```

Использования своего метода калькуляции:
```php
new Calculate(new CalcMethod());
```
`CalcMethod` должен имплеменить интерфейс `CalculationMethodInterface` и содержать метод `compute` 