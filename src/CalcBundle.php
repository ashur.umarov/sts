<?php
namespace STS\CalcBundle;

use STS\CalcBundle\Library\CalculationMethod\CalculationMethodInterface;
use STS\CalcBundle\Library\CalculationMethod\DefaultCalculationMethod;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CalcBundle extends Bundle
{

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->autowire(Library\Calculate::class)->addTag('calculate')->setPublic(true);
        $container->autowire(Controller\CalcController::class)->setPublic(true);
        $container->autowire(DefaultCalculationMethod::class);
        $container->setAlias(CalculationMethodInterface::class, DefaultCalculationMethod::class);
    }
}