<?php


namespace STS\CalcBundle\Library;

use STS\CalcBundle\Library\CalculationMethod\CalculationMethodInterface;
use STS\CalcBundle\Library\Entity\CalculatedEntity;

class Calculate
{

    /**
     * @var CalculationMethodInterface $method
     */
    private $method;

    /**
     * @param CalculationMethodInterface $method
     */
    public function __construct(CalculationMethodInterface $method)
    {
        $this->method = $method;
    }

    /**
     * @Route("/calc/{string}")
     * @param $string
     * @return mixed
     */
    public function calculate($string)
    {
        return $this->method->compute($string);
    }
}