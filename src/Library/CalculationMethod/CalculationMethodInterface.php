<?php


namespace STS\CalcBundle\Library\CalculationMethod;


use STS\CalcBundle\Library\Entity\CalculatedEntity;

interface CalculationMethodInterface
{

    /**
     * @param $string
     * @return mixed
     */
    public function compute($string);
}