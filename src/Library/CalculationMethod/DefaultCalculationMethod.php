<?php


namespace STS\CalcBundle\Library\CalculationMethod;


use STS\CalcBundle\Library\Entity\CalculatedEntity;

class DefaultCalculationMethod implements CalculationMethodInterface
{

    /**
     * @param $string
     * @return CalculatedEntity
     */
    public function compute($string)
    {
        $calcEntity = new CalculatedEntity();

        if (!preg_match_all('/^[0-9+\/\-*()]+$/m', $string, $matches)) {
            $calcEntity->error    = true;
            $calcEntity->errorMsg = "Pattern error: $string";
            return $calcEntity;
        }

        try {
            // eval не очень, но прогнал строку через регулярку
            $calcEntity->value = eval("return $string;");
        } catch (\Exception $error) {
            $calcEntity->error    = true;
            $calcEntity->errorMsg = "Pattern error: $error";
        }

        return $calcEntity;
    }
}