<?php

namespace STS\CalcBundle\Library\Entity;

class CalculatedEntity
{
    public $error = false;
    public $errorMsg;
    public $value = 0;
}