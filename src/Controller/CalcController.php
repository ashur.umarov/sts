<?php
namespace STS\CalcBundle\Controller;

use STS\CalcBundle\Library\Calculate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CalcController extends AbstractController
{

    /**
     * @var Calculate $calcService
     */
    private $calcService;

    /**
     * @param Calculate $calculate
     */
    public function __construct(Calculate $calculate)
    {
        $this->calcService = $calculate;
    }

    /**
     * @Route("/calc/{string}")
     * @param string $string
     * @return JsonResponse
     */
    public function calculate($string)
    {
        return $this->json($this->calcService->calculate($string));
    }
}