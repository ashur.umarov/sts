<?php

namespace STS\CalcBundle\Tests;

use \PHPUnit\Framework\TestCase;
use STS\CalcBundle\Library\Calculate;
use STS\CalcBundle\Library\CalculationMethod\DefaultCalculationMethod;

class CalculateTest extends TestCase
{

    /**
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = NULL, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @dataProvider expressionProvider
     * @param $exprs
     * @param $expected
     */
    public function testCalculate($exprs, $expected, $error)
    {
        $compute    = new DefaultCalculationMethod();
        $calculator = new Calculate($compute);
        $entity     = $calculator->calculate($exprs);
        $this->assertEquals($error, $entity->error);
        $this->assertEquals($expected, $entity->value);
    }

    /**
     * @return array
     */
    public function expressionProvider()
    {
        return array(
            array('1+2', 3, false),
            array('3-5', -2, false),
            array('2*(5+1)', 12, false),
            array('8/2+2', 6, false),
            array('8/2+2d', 0, true),
            array('%321+2%', 0, true),
            array('2/3', 0.66666666666667, false),
        );
    }
}